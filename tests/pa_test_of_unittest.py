__author__ = 'Sebastian Kalinowski'
__copyright___ = "Copyright (c) 2019 Sebastian Kalinowski"
__email__ = 'sebkal1997@@gmail.com'
__version__ = '0.1'

import unittest
import sys

from unittestmodule.pa_unittest import UnitTests

if "__main__" != __name__:
    sys.path.append("")


class TestUnitTests(unittest.TestCase):
    def setUp(self) -> None:
        self.unittests = UnitTests()
        self.number01 = None
        self.number02 = None
        self.text01 = None
        self.text02 = None
        self.container01 = None
        self.container02 = None
        self.object01 = None
        self.attribute01 = None

    def test_if_numbers_are_equal(self):
        # Assume
        self.number01 = 5
        self.number02 = 5
        # Action
        result = self.unittests.numbers_are_equal(self.number01, self.number02)
        # Assert
        self.assertTrue(result)

    def test_if_numbers_are_not_equal(self):
        # Assume
        self.number01 = 12
        self.number02 = 5
        # Action
        result = self.unittests.numbers_are_not_equal(self.number01, self.number02)
        # Assert
        self.assertTrue(result)

    def test_if_number_is_less_than(self):
        # Assume
        number01 = 2
        number02 = 7
        # Action
        result = self.unittests.number_is_less_than(number01, number02)
        # Assert
        self.assertTrue(result)

    def test_if_number_is_higher_than(self):
        # Assume
        self.number01 = 13
        self.number02 = 5
        # Action
        result = self.unittests.number_is_higher_than(self.number01, self.number02)
        # Assert
        self.assertTrue(result)

    def test_if_texts_are_equal(self):
        # Assume
        text01 = "Poland"
        text02 = "Poland"
        # Action
        result = self.unittests.texts_are_equal(text01, text02)
        # Assert
        self.assertTrue(result)

    def test_if_texts_are_not_equal(self):
        # Assume
        self.text01 = "Slowo"
        self.text02 = "slowo"
        # Action
        result = self.unittests.texts_are_not_equal(self.text01, self.text02)
        # Assert
        self.assertTrue(result)

    def test_if_text_begins_at_another(self):
        # Assume
        self.text01 = "Poland"
        self.text02 = "Pol"
        # Action
        result = self.unittests.text_begins_at_another(self.text01, self.text02)
        # Assert
        self.assertTrue(result)

    def test_if_text_ends_at_another(self):
        # Assume
        self.text01 = "Poland"
        self.text02 = "and"
        # Action
        result = self.unittests.text_ends_at_another(self.text01, self.text02)
        # Assert
        self.assertTrue(result)

    def test_if_list_are_same_length(self):
        # Assume
        self.container01 = [2, 3, 4, 11]
        self.container02 = [2, 3, 4, 12]
        # Action
        result = self.unittests.containers_are_same_length(self.container01, self.container02)
        # Assert
        self.assertTrue(result)

    def test_if_tuple_are_same_length(self):
        # Assume
        self.container01 = (2, 3, 4, 11)
        self.container02 = (2, 3, 4, 12)
        # Action
        result = self.unittests.containers_are_same_length(self.container01, self.container02)
        # Assert
        self.assertTrue(result)

    def test_if_dictonaries_are_same_length(self):
        # Assume
        self.container01 = {1: 2, 3: "cat", "dog": "meow"}
        self.container02 = {1: 2, 3: "cat", "dog": "meow"}
        # Action
        result = self.unittests.dictonaries_are_same_length(self.container01, self.container02)
        # Assert
        self.assertTrue(result)

    def test_if_list_contains_same_content(self):
        # Assume
        self.container01 = [2, 3, 4, 12]
        self.container02 = [2, 3, 4, 12]
        # Action
        result = self.unittests.containers_contains_same_content(self.container01, self.container02)
        # Assert
        self.assertTrue(result)

    def test_if_tuple_contains_same_content(self):
        # Assume
        self.container01 = (2, 3, 4, 11)
        self.container02 = (2, 3, 4, 11)
        # Action
        result = self.unittests.containers_contains_same_content(self.container01, self.container02)
        # Assert
        self.assertTrue(result)

    def test_if_dictonaries_contains_same_content(self):
        # działa tylko dla jednego typu klucza@@@@@@
        # Assume
        self.container01 = {1: 2, 3: "cat", 2: "meow"}
        self.container02 = {1: 2, 3: "cat", 2: "meow"}
        # Action
        result = self.unittests.dictonaries_contains_same_content(self.container01, self.container02)
        # Assert
        self.assertTrue(result)

    def test_if_list_contains_data_at_same_key(self):
        # Assume
        self.container01 = [2, 3, 4, 12]
        self.container02 = [2, 3, 4, 12]
        # Action
        result = self.unittests.containers_contains_data_at_same_key(self.container01, self.container02)
        # Assert
        self.assertTrue(result)

    def test_if_tuple_contains_data_at_same_key(self):
        # Assume
        self.container01 = (2, 3, 4, 11)
        self.container02 = (2, 3, 4, 11)
        # Action
        result = self.unittests.containers_contains_data_at_same_key(self.container01, self.container02)
        # Assert
        self.assertTrue(result)

    def test_if_dictonaries_contains_data_at_same_key(self):
        # Assume
        self.container01 = {1: 2, 3: "cat", "dog": "meow"}
        self.container02 = {1: 2, 3: "cat", "dog": "meow"}
        # Action
        result = self.unittests.dictonaries_contains_data_at_same_key(self.container01, self.container02)
        # Assert
        self.assertTrue(result)

    def test_if_objects_has_method_or_instance(self):
        # Assume
        self.object01 = 13
        self.attribute01 = "bit_length"
        # Action
        result = self.unittests.objects_has_method_or_instance(self.object01, self.attribute01)
        # Assert
        self.assertTrue(result)

    def test_if_objects_is_none(self):
        # Assume
        self.object01 = None
        # Action
        result = self.unittests.objects_is_none(self.object01)
        # Assert
        self.assertTrue(result)

    def test_if_objects_is_not_none(self):
        # Assume
        self.object01 = 11
        # Action
        result = self.unittests.objects_is_not_none(self.object01)
        # Assert
        self.assertTrue(result)
