__author__ = 'Sebastian Kalinowski'
__copyright___ = "Copyright (c) 2019 Sebastian Kalinowski"
__email__ = 'sebkal1997@@gmail.com'
__version__ = '0.1'


class UnitTests:
    def __init__(self):
        self.test_value = 0
        self.test_true_value = 0
        self.test_false_value = 0

    def __del__(self):
        print("You have ran {} test. {} was positive and {} was negative".format(self.test_value, self.test_true_value,
                                                                                 self.test_false_value))
        self.test_value = 0
        self.test_true_value = 0
        self.test_false_value = 0

    def numbers_are_equal(self, number01: int, number02: int) -> bool:
        self.test_value = self.test_value + 1
        if number01 == number02:
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def numbers_are_not_equal(self, number01: int, number02: int) -> bool:
        self.test_value = self.test_value + 1
        if number01 != number02:
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def number_is_less_than(self, number01: int, number02: int) -> bool:
        self.test_value = self.test_value + 1
        if number01 < number02:
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def number_is_higher_than(self, number01: int, number02: int) -> bool:
        self.test_value = self.test_value + 1
        if number01 > number02:
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def texts_are_equal(self, text01: str, text02: str) -> bool:
        self.test_value = self.test_value + 1
        if text01 == text02:
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def texts_are_not_equal(self, text01: str, text02: str) -> bool:
        self.test_value = self.test_value + 1
        if text01 != text02:
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def text_begins_at_another(self, text01: str, text02: str) -> bool:
        self.test_value = self.test_value + 1
        if text01.startswith(text02):
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def text_ends_at_another(self, text01: str, text02: str) -> bool:
        self.test_value = self.test_value + 1
        if text01.endswith(text02):
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def containers_are_same_length(self, container01: iter, container02: iter) -> bool:
        self.test_value = self.test_value + 1
        if len(container01) == len(container02):
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def dictonaries_are_same_length(self, container01: dict, container02: dict) -> bool:
        self.test_value = self.test_value + 1
        if len(container01) == len(container02):
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def containers_contains_same_content(self, container01: iter, container02: iter) -> bool:
        self.test_value = self.test_value + 1
        sorted(container01)
        sorted(container02)
        if container01 == container02:
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def dictonaries_contains_same_content(self, container01: dict, container02: dict) -> bool:
        self.test_value = self.test_value + 1
        sorted(container01)
        sorted(container02)
        if container01 == container02:
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def containers_contains_data_at_same_key(self, container01: iter, container02: iter) -> bool:
        self.test_value = self.test_value + 1
        if len(container01) == len(container02):
            self.test_true_value = self.test_true_value + 1
            iteration = 0
            while iteration < len(container01):
                if container01[iteration] == container02[iteration]:
                    iteration = iteration + 1
                else:
                    return False
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def dictonaries_contains_data_at_same_key(self, container01: dict, container02: dict) -> bool:
        self.test_value = self.test_value + 1
        if len(container01) == len(container02):
            self.test_true_value = self.test_true_value + 1
            iteration = 0
            while iteration < len(container01):
                key1 = list(container01.keys())
                key2 = list(container02.keys())
                if container01[key1[iteration]] == container02[key2[iteration]]:
                    return True
                else:
                    return False
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def objects_has_method_or_instance(self, object01: object, attribute01: str) -> bool:
        self.test_value = self.test_value + 1
        if hasattr(object01, attribute01):
            # callable for check is it method
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def objects_is_none(self, object01: object) -> bool:
        self.test_value = self.test_value + 1
        if object01 is None:
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False

    def objects_is_not_none(self, object01: object) -> bool:
        self.test_value = self.test_value + 1
        if object01 is not None:
            self.test_true_value = self.test_true_value + 1
            return True
        else:
            self.test_false_value = self.test_false_value + 1
            return False
